# Tags
locals {
  common_tags = {
    Reference = "Created by Guenael Voisin for JDETRIAL  POC"
  }
}

# Available regions for this subscription
data "oci_identity_region_subscriptions" "this" {
  tenancy_id = var.tenancy_ocid
}

# Available Services
data "oci_core_services" "all_services" {
  filter {
    name   = "name"
    values = ["All .* Services In Oracle Services Network"]
    regex  = true
  }
}

data oci_identity_availability_domain region_ad3 {
  compartment_id = lookup(oci_identity_compartment.jdetrial-compartment,"id")
  ad_number      = "1"
}


data "oci_marketplace_listings" "jdetrial_mkplace" {
  name = [var.jdetrial_marketplace_name]
  compartment_id = lookup(oci_identity_compartment.jdetrial-compartment,"id")
}

data "oci_marketplace_listing_package_agreements" "jdetrial_mkplace_package_agreements" {
  #Required
  listing_id      = data.oci_marketplace_listing.jdetrial_mkplace.id
  package_version = data.oci_marketplace_listing.jdetrial_mkplace.default_package_version

  #Optional
  compartment_id = lookup(oci_identity_compartment.jdetrial-compartment,"id")
}

data "oci_marketplace_listing_package" "jdetrial_mkplace_package" {
  #Required
  listing_id      = data.oci_marketplace_listing.jdetrial_mkplace.id
  package_version = data.oci_marketplace_listing.jdetrial_mkplace.default_package_version
#  package_version  = "Oracle-E-Business-Suite-Vision-Image-12.2.10-19C-ECC-03-11-2021"

  #Optional
  compartment_id = lookup(oci_identity_compartment.jdetrial-compartment,"id")
}

data "oci_marketplace_listing_packages" "jdetrial_mkplace_packages" {
  #Required
  listing_id = data.oci_marketplace_listing.jdetrial_mkplace.id

  #Optional
  compartment_id = lookup(oci_identity_compartment.jdetrial-compartment,"id")
}

data "oci_marketplace_listing" "jdetrial_mkplace" {
  listing_id     = data.oci_marketplace_listings.jdetrial_mkplace.listings[0].id
  compartment_id = lookup(oci_identity_compartment.jdetrial-compartment,"id")
}

data "oci_core_app_catalog_listing_resource_version" "jdetrial_catalog_listing" {
  listing_id       = data.oci_marketplace_listing_package.jdetrial_mkplace_package.app_catalog_listing_id
  resource_version = data.oci_marketplace_listing_package.jdetrial_mkplace_package.app_catalog_listing_resource_version
}



# Cloud Init
#data "template_cloudinit_config" "nodes" {
#  gzip          = true
#  base64_encode = true
#
#  part {
#    filename     = "cloud-config.yaml"
#    content_type = "text/cloud-config"
#    content      = local.cloud_init
#  }
#}

locals {
  setup_jdedwards = templatefile("${path.module}/scripts/jdedwards.template",
    {
      jdehostname = "jdetrial${var.environment}"
      jdeip = oci_core_instance.jdetrial_vm.public_ip
      #jdeip = "test"
      jdedbsyspwd = var.jdedbsyspwd
      jdedbuserpwd = var.jdedbuserpwd
      jdewlspwd = var.jdewlspwd
  })

#  cloud_init = templatefile("${path.module}/scripts/cloud-config.template.yaml",
#    {
#      setup_jdedwards_sh_content     = base64gzip(local.setup_jdedwards)
#  })
}

