# CHANGELOG

2022-06-02 (v0.0.1)

- Terraform OCI Provider Updated to the latest
- Terraform scripts to deploy JDE Trial Edition on OCI
