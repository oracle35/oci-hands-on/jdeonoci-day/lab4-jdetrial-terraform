# OCI Provider
variable "tenancy_ocid" {}
variable "region" {}
variable "home_region" {}
variable "user_ocid" {
  default = ""
}
variable "fingerprint" {
  default = ""
}
variable "private_key_path" {
  default = ""
}

variable "instance_shape" {
  default = "VM.Standard.E4.Flex"
}
variable "instance_ocpus" {
  default = 2
}
variable "instance_shape_config_memory_in_gbs" {
  default = 32
}

variable "instance_public_sshkey" {
  default = ""
}

variable "compartment_parent_ocid" {
  default = ""
}

variable "compartment_name" {
  default = "comp-jdetrial"
}

variable "network_cidrs" {
  type = map(string)

  default = {
    MAIN-VCN-CIDR                = "10.1.0.0/16"
    WEB-SUBNET-REGIONAL-CIDR     = "10.1.1.0/24"
    ALL-CIDR                     = "0.0.0.0/0"
    HOME-CIDR                    = "82.65.89.141/32"
  }
}

variable "environment" {
  default = "dev"
}

variable "lb_shape" {
  default = "flexible"
}
variable "lb_shape_details_minimum_bandwidth_in_mbps" {
  default = 10
}
variable "lb_shape_details_maximum_bandwidth_in_mbps" {
  default = 100
}

variable "lb_private_key" {
  default = ""
}
variable "lb_public_certificate" {
  default = ""
}

# WAF
variable "use_waf" {
  default = false
}
variable "lb_https" {
  default = false
}

locals {
  instance_shape                             = var.instance_shape
  lb_shape                                   = var.lb_shape
  lb_shape_details_minimum_bandwidth_in_mbps = var.lb_shape_details_minimum_bandwidth_in_mbps
  lb_shape_details_maximum_bandwidth_in_mbps = var.lb_shape_details_maximum_bandwidth_in_mbps
  region_to_deploy                           = var.region
}


variable "jdetrial_marketplace_name" {
  default =  ""
}

variable "jdetrial_fault_domain" {
  default = "FAULT-DOMAIN-3"
}

variable "jdedbsyspwd" {
  default =  ""
}
variable "jdedbuserpwd" {
  default =  ""
}
variable "jdewlspwd" {
  default =  ""
}


variable "instance_private_key" {
  default =  ""
}
variable "timeout" {
  description = "Timeout setting for resource creation "
  default     = "30m"
}
