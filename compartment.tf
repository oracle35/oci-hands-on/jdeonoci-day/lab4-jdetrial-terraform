resource "oci_identity_compartment" "jdetrial-compartment" {
    # Required
    # compartment_id = var.tenancy_ocid
    provider = oci.home_region
    compartment_id = var.compartment_parent_ocid
    description = "Compartment for JDE Trial resources."
    name = var.compartment_name
}
